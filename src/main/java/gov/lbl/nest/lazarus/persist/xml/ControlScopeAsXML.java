package gov.lbl.nest.lazarus.persist.xml;

import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import gov.lbl.nest.lazarus.control.ControlFlowImpl;
import gov.lbl.nest.lazarus.control.ControlOnlyProcessImpl;
import gov.lbl.nest.lazarus.control.ControlOwner;
import gov.lbl.nest.lazarus.execution.TerminationListener;
import gov.lbl.nest.lazarus.execution.ThrownFailure;
import gov.lbl.nest.lazarus.process.ControlledDataScope;

/**
 * This class extends the {@link ControlledDataScope} class in order to preserve
 * its state in an XML file.
 *
 * @author patton
 */
@XmlType(propOrder = { "uri",
                       "labelValue",
                       "state",
                       "priorityValue",
                       "controlFlowsAsXML",
                       "dataScopeAsXML" })
@XmlRootElement(name = "workflow_state")
public class ControlScopeAsXML extends
                               ControlledDataScope {

    /**
     * The set of lifecycle state for this object.
     * 
     * @author patton
     *
     */
    @XmlEnum
    public enum STATE {
                       /**
                        * This object has been fully created, but not started execution.
                        */
                       CREATED,

                       /**
                        * This object has started execution, but is not yet terminating.
                        */
                       EXECUTING,

                       /**
                        * This object is terminating, but not yet terminated.
                        */
                       TERMINATING,
    }

    /**
     * The collection of {@link ControlFlowAsXML} instances that currently exist.
     */
    private List<ControlFlowAsXML> flowsAsXML = new ArrayList<>();

    /**
     * The current lifecycle {@link STATE} instance of this object.
     */
    private STATE state;

    /**
     * The URI identifying this instance of a workflow.
     */
    private URI uri;

    /**
     * Creates an instance of this class.
     */
    protected ControlScopeAsXML() {
        // Required by JAXB
    }

    @Override
    public ControlFlowImpl createControlFlow() {
        final ControlFlowAsXML result = new ControlFlowAsXML(this);
        flowsAsXML.add(result);
        update();
        return result;
    }

    @Override
    protected ThrownFailure createThrownFailure(ControlFlowImpl arg0,
                                                Throwable arg1) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Returns the collection of {@link ControlFlowAsXML} instances that currently
     * exist.
     *
     * @return the collection of {@link ControlFlowAsXML} instances that currently
     *         exist.
     */
    @XmlElement(name = "control_flow")
    @XmlElementWrapper(name = "control_flows")
    protected List<ControlFlowAsXML> getControlFlowsAsXML() {
        return flowsAsXML;
    }

    /**
     * Returns the {@link DataScopeAsXML} instance used by this object.
     *
     * @return the {@link DataScopeAsXML} instance used by this object.
     */
    @XmlElement(name = "data_objects")
    protected DataScopeAsXML getDataScopeAsXML() {
        return (DataScopeAsXML) getDataScope();
    }

    /**
     * Returns the {@link String} instance describing this object.
     *
     * @return the {@link String} instance describing this object.
     */
    @XmlElement(name = "label")
    protected String getLabelValue() {
        return getLabel();
    }

    /**
     * Returns the {@link Integer} instance containing the priority this object.
     *
     * @return the {@link Integer} instance containing the priority this object.
     */
    @XmlElement(name = "priority")
    protected Integer getPriorityValue() {
        return getPriority();
    }

    /**
     * Returns the current lifecycle {@link STATE} instance of this object.
     * 
     * @return the current lifecycle {@link STATE} instance of this object.
     */
    @XmlElement
    protected STATE getState() {
        return state;
    }

    /**
     * Returns the URI identifying this instance of a workflow.
     * 
     * @return the URI identifying this instance of a workflow.
     */
    @XmlElement
    protected URI getUri() {
        return uri;
    }

    @Override
    protected boolean isFailed() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    protected boolean isTerminating() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    protected void preserve() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void purge() {
        // TODO: Delete this ojbect6 from backing store.

    }

    /**
     * Removes the supplied {@link ControlFlowAsXML} instance from this object.
     *
     * @param controlFlowAsXML
     *            the {@link ControlFlowAsXML} instance to be removed.
     */
    void remove(ControlFlowAsXML controlFlowAsXML) {
        synchronized (flowsAsXML) {
            final Iterator<ControlFlowAsXML> iterator = flowsAsXML.iterator();
            while (iterator.hasNext()) {
                if (iterator.next() == controlFlowAsXML) {
                    iterator.remove();
                    update();
                    return;
                }
            }
            throw new IllegalStateException("Attempting to pruge a Control flow not ownd by this object");
        }
    }

    /**
     * Sets the control elements of the object
     * 
     * @param process
     *            the {@link ControlOnlyProcessImpl} instance of the workflow to be
     *            executed.
     * @param listener
     *            the {@link TerminationListener} instance, if any, to callback when
     *            this object has terminated.
     */
    final void setControlElementsForXml(ControlOnlyProcessImpl process,
                                        TerminationListener listener) {
        setControlElements(process,
                           listener);
        final Map<String, ? extends ControlOwner> owners = process.getOwners();
        if (null != flowsAsXML && !flowsAsXML.isEmpty()) {
            for (ControlFlowAsXML flowAsXML : flowsAsXML) {
                flowAsXML.setControlElements(this,
                                             owners.get(flowAsXML.getOwnerIdentity()));
                flowAsXML.setOwnerIdentity(null);
                addControlFlow(flowAsXML);
            }
        }
    }

    /**
     * Sets the collection of {@link ControlFlowAsXML} instances that currently
     * exist.
     *
     * @param flowsAsXML
     *            the collection of {@link ControlFlowAsXML} instances that
     *            currently exist.
     */
    protected void setControlFlowsAsXML(List<ControlFlowAsXML> flowsAsXML) {
        this.flowsAsXML = flowsAsXML;
    }

    @Override
    protected void setCreated() {
        setState(STATE.CREATED);
    }

    /**
     * Sets the {@link DataScopeAsXML} instance used by this object.
     *
     * @param scope
     *            the {@link DataScopeAsXML} instance used by this object.
     */
    protected void setDataScopeAsXml(DataScopeAsXML scope) {
        setDataScope(scope);
    }

    @Override
    protected void setExecuting() {
        setState(STATE.EXECUTING);
    }

    /**
     * Sets the {@link String} instance describing this object.
     *
     * @param label
     *            the {@link String} instance describing this object.
     */
    protected void setLabelValue(String label) {
        setLabel(label);
    }

    /**
     * Sets the {@link Integer} instance containing the priority this object.
     *
     * @param priority
     *            the {@link Integer} instance containing the priority this object.
     */
    protected void setPriorityValue(Integer priority) {
        setPriority(priority);
    }

    /**
     * Sets the current lifecycle {@link STATE} instance of this object.
     * 
     * @param state
     *            the current lifecycle {@link STATE} instance of this object.
     */
    protected void setState(STATE state) {
        this.state = state;
    }

    @Override
    protected void setTerminating() {
        setState(STATE.TERMINATING);
    }

    /**
     * Sets the URI identifying this instance of a workflow.
     * 
     * @param uri
     *            the URI identifying this instance of a workflow.
     */
    protected void setUri(URI uri) {
        this.uri = uri;
    }

    /**
     * Replaces any existing file containing this object with an updated copy.
     */
    void update() {
        // TODO: Write this.
    }

    @Override
    protected void updatedFailingToFailed() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void updateFailures(List<? extends ThrownFailure> arg0) {
        // TODO Auto-generated method stub

    }
}
