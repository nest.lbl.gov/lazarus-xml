package gov.lbl.nest.lazarus.persist.xml;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.xml.item.ValueAdapter;
import gov.lbl.nest.common.xml.item.ValueAsString;
import gov.lbl.nest.common.xml.item.ValueCollection;
import gov.lbl.nest.lazarus.data.DataFactory;
import gov.lbl.nest.lazarus.data.DataObjectImpl;
import gov.lbl.nest.lazarus.data.DataScope;
import gov.lbl.nest.lazarus.data.ItemDefinitionImpl;

/**
 * This class extends the {@link DataFactory} class in order to preserve the
 * state in an instance as an XML file.
 *
 * @author patton
 */
public class DataImplFactory extends
                             DataFactory {

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(DataImplFactory.class);

    @Override
    public DataScope createDataScope(Collection<? extends DataObjectImpl> dataObjects,
                                     Object guidance) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public DataScope recoverDataScope(Collection<? extends DataObjectImpl> dataObjects,
                                      Object guidance) {
        final Class<?>[] classList = getPersistentClasses(dataObjects);

        final Path path = (Path) guidance;
        try {
            JAXBContext jc = JAXBContext.newInstance(classList);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            final DataScopeAsXML dataScope = (DataScopeAsXML) unmarshaller.unmarshal(path.toFile());
            dataScope.setDataElements(dataObjects,
                                      classList);
            dataScope.recover();
            return dataScope;
        } catch (JAXBException e) {
            LOG.error("Can not build Unmarshall DataScope at \"" + path
                      + "\"");
        }
        return null;
    }

    /**
     * Returns the collection of {@link Class} instances needed to marshall and
     * unmarshall data in a {@link DataObjectAsXML} object.
     * 
     * @param dataObjects
     *            the collection of {@link DataObjectImpl} instance in the
     *            {@link DataObjectAsXML} instance.
     * 
     * @return the collection of {@link Class} instances needed to marshall and
     *         unmarshall data in a {@link DataObjectAsXML} object.
     */
    private static Class<?>[] getPersistentClasses(Collection<? extends DataObjectImpl> dataObjects) {
        final List<Class<?>> classList = new ArrayList<>(Arrays.asList(new Class<?>[] { DataScopeAsXML.class }));
        for (DataObjectImpl dataObject : dataObjects) {
            final ItemDefinitionImpl itemSubject = dataObject.getItemSubject();
            final Class<?> structure = itemSubject.getStructure();
            final Class<?> structureToUse;
            if (itemSubject.isCollection()) {
                structureToUse = ValueCollection.class;
            } else if ((ValueAdapter.getNonJAXBClasses()).contains(structure)) {
                structureToUse = ValueAsString.class;
            } else {
                structureToUse = structure;
            }
            if (!classList.contains(structureToUse)) {
                classList.add(structureToUse);
            }
        }
        return classList.toArray(new Class<?>[0]);
    }

}
