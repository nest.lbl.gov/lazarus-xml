package gov.lbl.nest.lazarus.persist.xml;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * This class is used to collect all of the XML files that have numerical names.
 *
 * @author patton
 *
 * @param <T>
 *            The type of Path being iterated over.
 */
class GatherNumericXMLFiles<T extends Path> implements
                           FileVisitor<T> {

    /**
     * The suffix used for XML files.
     */
    private final String XML_SUFFIX = ".xml";

    /**
     * The length of the suffix used for XML files.
     */
    private final int XML_SUFFIX_LENGTH = XML_SUFFIX.length();

    /**
     * The {@link Comparator} instance used to order the results.
     */
    private final Comparator<T> NUMERICAL_ORDER = new Comparator<>() {

        @Override
        public int compare(T o1,
                           T o2) {
            // Filenames guaranteed to be Integers do to selection.
            final int int1 = xmlFileAsInteger(o1);
            final int int2 = xmlFileAsInteger(o2);
            return int1 - int2;
        }
    };

    /**
     * The sequence of {@link Path} instance that have numerical names.
     */
    private final List<T> paths = new ArrayList<>();

    /**
     * Returns the sequence of {@link Path} instance that have numerical names.
     *
     * @return the sequence of {@link Path} instance that have numerical names.
     */
    List<T> getPaths() {
        Collections.sort(paths,
                         NUMERICAL_ORDER);
        return paths;
    }

    @Override
    public final FileVisitResult postVisitDirectory(T dir,
                                                    IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public final FileVisitResult preVisitDirectory(T dir,
                                                   BasicFileAttributes attrs) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public final FileVisitResult visitFile(T file,
                                           BasicFileAttributes attrs) throws IOException {
        if (null != xmlFileAsInteger(file)) {
            paths.add(file);
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public final FileVisitResult visitFileFailed(T file,
                                                 IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    /**
     * Returns the {@link Integer} instance of the file name, or <code>null</code>
     * if it is not an integer.
     * 
     * @param path
     *            the {@link Path} instance to return as an {@link Integer}
     *            instance.
     * 
     * @return the {@link Integer} instance of the file name, or <code>null</code>
     *         if it is not an integer.
     */
    private final Integer xmlFileAsInteger(T path) {
        final String name = (path.getFileName()).toString();
        try {
            if (name.endsWith(XML_SUFFIX)) {
                return Integer.parseInt(name.substring(0,
                                                       name.length() - XML_SUFFIX_LENGTH));
            }
        } catch (NumberFormatException e) {
            // Do nothing, skip.
        }
        return null;
    }

}
