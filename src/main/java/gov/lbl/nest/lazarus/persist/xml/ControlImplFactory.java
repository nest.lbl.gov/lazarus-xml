package gov.lbl.nest.lazarus.persist.xml;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.CustomConfigPath;
import gov.lbl.nest.lazarus.control.ControlFactory;
import gov.lbl.nest.lazarus.control.ControlOnlyProcessImpl;
import gov.lbl.nest.lazarus.control.ControlScope;
import gov.lbl.nest.lazarus.execution.TerminationListener;

/**
 * This class extends the {@link ControlFactory} class in order to preserve the
 * state in an instance as an XML file.
 *
 * @author patton
 */
public class ControlImplFactory extends
                                ControlFactory {

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ControlImplFactory.class);

    /**
     * The name of the resource that may contain the path to an alternate
     * <q>suspend<\q> file for SPADE.
     */
    private static final String LAZARUS_ROOT_RESOURCE = "lazarus_root";

    /**
     * True if the {@link #configFileName} value has been loaded.
     */
    private static final CustomConfigPath CONFIG_PATH = new CustomConfigPath(ControlledDataImplFactory.class,
                                                                             LAZARUS_ROOT_RESOURCE);

    /**
     * The name of the directory containing the data for the active
     * {@link ControlScopeAsXML} instances using this object.
     */
    protected static final Path ACTIVE_CONTROLS_DIRECTORY = Paths.get("active");

    /**
     * The {@link Path} instance used as the root storage by this object.
     */
    private Path lazarusRoot;

    @Override
    public ControlScope createControlScope(ControlOnlyProcessImpl arg0,
                                           String arg1,
                                           Integer arg2,
                                           TerminationListener arg3) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Returns the {@link Path} instance used as the root storage by this object.
     *
     * @return the {@link Path} instance used as the root storage by this object.
     */
    private Path getLazarusRoot() {
        if (null == lazarusRoot) {
            final Path configPath = CONFIG_PATH.getConfigPath();
            final Path configPathToUse;
            if (null == configPath) {
                configPathToUse = Paths.get("lazarus");
            } else {
                configPathToUse = configPath;
            }
            lazarusRoot = configPathToUse;
        }
        return lazarusRoot;
    }

    /**
     * Returns a collection of {@link Path} instance which contain
     * {@link ControlScope} states for {@link ControlScope} instances that were in
     * the process of being executed when the previous instance of this application
     * stopped. active when the application
     *
     * @param process
     *            the name of the process for which to recover the paths.
     *
     * @return the collection of {@link Path} instance for the recovered
     *         {@link ControlScope} instances.
     */
    private Collection<Path> recoverControlScopePaths(String process) {
        final Path flowDirectory = (getLazarusRoot().resolve(process)).resolve(ACTIVE_CONTROLS_DIRECTORY);
        final GatherNumericXMLFiles<Path> numericFiles = new GatherNumericXMLFiles<>();
        try {
            Files.walkFileTree(flowDirectory,
                               new HashSet<FileVisitOption>(),
                               1,
                               numericFiles);
            return numericFiles.getPaths();
        } catch (IOException e) {
            throw new IllegalStateException("Failure in backing store!",
                                            e);
        }
    }

    @Override
    public Collection<? extends ControlScope> recoverControlScopes(ControlOnlyProcessImpl process,
                                                                   TerminationListener listener) {
        final Collection<Path> paths = recoverControlScopePaths(process.getName());
        final List<ControlScopeAsXML> results = new ArrayList<>();
        final Iterator<Path> iterator = paths.iterator();
        try {
            JAXBContext jc = JAXBContext.newInstance(ControlScopeAsXML.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            while (iterator.hasNext()) {
                final Path path = iterator.next();
                try {
                    final ControlScopeAsXML controlScope = (ControlScopeAsXML) unmarshaller.unmarshal(path.toFile());
                    controlScope.setControlElementsForXml(process,
                                                          listener);
                    results.add(controlScope);
                } catch (JAXBException e) {
                    if (null == System.getProperty("gov.lbl.nest.lazarus.persist.xml.suppressTracebacks")) {
                        LOG.error("Unable to recover the ControlScope in \"" + path
                                  + "\"");
                    }
                }
            }
        } catch (JAXBException e) {
            LOG.error("Can not build Unmarshaller");
        }
        return results;
    }

}
