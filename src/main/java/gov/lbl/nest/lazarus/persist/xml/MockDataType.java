package gov.lbl.nest.lazarus.persist.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class is used to exercise the storage and retrieval of complex types.
 * 
 * @author patton
 */
@XmlRootElement(name = "mock_data")
@XmlType(propOrder = { "number",
                       "string" })
public class MockDataType {

    /**
     * The {@link Integer} instance used by this object.
     */
    private Integer number;

    /**
     * The {@link String} instance used by this object.
     */
    private String string;

    /**
     * Returns the {@link Integer} instance used by this object.
     * 
     * @return the {@link Integer} instance used by this object.
     */
    @XmlElement
    protected Integer getNumber() {
        return number;
    }

    /**
     * Returns the {@link String} instance used by this object.
     * 
     * @return the {@link String} instance used by this object.
     */
    @XmlElement
    protected String getString() {
        return string;
    }

    /**
     * Sets the {@link Integer} instance used by this object.
     * 
     * @param number
     *            the {@link Integer} instance used by this object.
     */
    protected void setNumber(Integer number) {
        this.number = number;
    }

    /**
     * Sets the {@link String} instance used by this object.
     * 
     * @param string
     *            the {@link String} instance used by this object.
     */
    protected void setString(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return number + ":"
               + string;
    }
}
