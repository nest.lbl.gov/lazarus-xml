package gov.lbl.nest.lazarus.persist.xml;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import gov.lbl.nest.lazarus.control.ControlFlowException;
import gov.lbl.nest.lazarus.control.ControlFlowImpl;
import gov.lbl.nest.lazarus.control.ControlOwner;
import gov.lbl.nest.lazarus.control.SequenceFlowImpl;

/**
 * This class extends the {@link ControlFlowImpl} class in order to preserve its
 * state in an XML file.
 *
 * @author patton
 */
@XmlType(propOrder = { "ownerIdentity",
                       "fanning" })
class ControlFlowAsXML extends
                       ControlFlowImpl {

    /**
     * The {@link String} instance used to identify the current owner of this
     * object.
     */
    private String ownerIdentity;

    /**
     * Creates an instance of this class.
     */
    protected ControlFlowAsXML() {
        // Required by JAXB
        super(null,
              null,
              -1);
    }

    /**
     * Creates an instance of this class.
     *
     * @param controlScope
     *            the {@link ControlScopeAsXML} instance to which this object
     *            belongs.
     */
    ControlFlowAsXML(ControlScopeAsXML controlScope) {
        super(controlScope,
              null,
              -1);
    }

    /**
     * Returns the index, if this object is a forked instance, of the
     * {@link SequenceFlowImpl} to which this object should be transferred,
     * otherwise returns -1.
     *
     * @return the index, if this object is a forked instance, of the
     *         {@link SequenceFlowImpl} to which this object should be transferred.
     */
    @XmlElement(name = "fanning_index")
    protected Integer getFanning() {
        int fanningIndex = getFanningIndex();
        if (-1 == fanningIndex) {
            return null;
        }
        return fanningIndex;
    }

    /**
     * Returns the {@link String} instance used to identify the current owner of
     * this object.
     *
     * @return the {@link String} instance used to identify the current owner of
     *         this object.
     */
    @XmlElement(name = "owner")
    protected String getOwnerIdentity() {
        if (null == ownerIdentity) {
            final ControlOwner owner = getOwner();
            if (null == owner) {
                return null;
            }
            return owner.getOwnerIdentity();
        }
        return ownerIdentity;
    }

    @Override
    public List<Integer> getRecoveredBranches() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void purge() {
        ((ControlScopeAsXML) getControlScope()).remove(this);
    }

    @Override
    public void setBranchesToRecover(List<Integer> branchesToExecute) throws ControlFlowException {
        // TODO Auto-generated method stub

    }

    /**
     * Sets the index, if this object is a forked instance, of the
     * {@link SequenceFlowImpl} to which this object should be transferred.
     * 
     * @param index
     *            the index, if this object is a forked instance, of the
     *            {@link SequenceFlowImpl} to which this object should be
     *            transferred.
     */
    protected void setFanning(Integer index) {
        if (null == index) {
            setFanningIndex(-1);
        } else {
            setFanningIndex(index);
        }
    }

    /**
     * Sets the {@link String} instance used to identify the current owner of this
     * object.
     * 
     * @param identity
     *            the {@link String} instance used to identify the current owner of
     *            this object.
     */
    protected void setOwnerIdentity(String identity) {
        ownerIdentity = identity;
    }

    @Override
    protected void updateForkedOwner(ControlOwner owner,
                                     int fanningIndex) {
        // Owner identity and Fanning Index already set in ControlFlowImpl
        ((ControlScopeAsXML) getControlScope()).update();
    }

    @Override
    protected void updateJoiningOwner(ControlOwner owner,
                                      int fanningIndex) {
        // Owner identity and Fanning Index already set in ControlFlowImpl
        ((ControlScopeAsXML) getControlScope()).update();
    }

    @Override
    protected void updateOwner(ControlOwner owner) {
        // Owner identity already set in ControlFlowImpl
        ((ControlScopeAsXML) getControlScope()).update();
    }

}
