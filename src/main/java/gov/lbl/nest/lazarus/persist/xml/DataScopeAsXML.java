package gov.lbl.nest.lazarus.persist.xml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import gov.lbl.nest.common.xml.item.NullValue;
import gov.lbl.nest.common.xml.item.ValueAsString;
import gov.lbl.nest.common.xml.item.ValueCollection;
import gov.lbl.nest.lazarus.data.DataCell;
import gov.lbl.nest.lazarus.data.DataFlowException;
import gov.lbl.nest.lazarus.data.DataObjectImpl;
import gov.lbl.nest.lazarus.data.DataOnlyActivityImpl;
import gov.lbl.nest.lazarus.data.DataScope;
import gov.lbl.nest.lazarus.structure.DataObject;
import gov.lbl.nest.lazarus.structure.DataObjectException;

/**
 * This class extends the {@link DataScope} class in order to preserve its state
 * in an XML file
 *
 * @author patton
 */
@XmlRootElement(name = "data_objects")
@XmlType(propOrder = { "dataObjectAsXMLs" })
public class DataScopeAsXML extends
                            DataScope {

    /**
     * The collection {@link DataObjectAsXML} instance are in this object, indexed
     * by identity.
     */
    private List<DataObjectAsXML> dataObjectAsXMLs = new ArrayList<>();

    /**
     * The collection {@link DataObjectAsXML} instance are in this object, indexed
     * by identity.
     */
    private Map<String, DataObjectAsXML> dataObjectByIdentity;

    /**
     * The collection of {@link Class} instances need to write this object.
     */
    private Class<?>[] persistentClasses;

    /**
     * Creates an instance of this class.
     */
    protected DataScopeAsXML() {
        // Required by JAXB
        super(null);
    }

    @Override
    public DataCell createDataCell(DataOnlyActivityImpl activity,
                                   Object guidance) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void createOrReplaceCollectionValues(String identity,
                                                   Collection<?> values) throws DataObjectException {
        // TODO Auto-generated method stub

    }

    @Override
    protected void createOrReplaceValue(String identity,
                                        Object value) throws DataObjectException {
        // TODO Auto-generated method stub

    }

    /**
     * Returns the collection {@link DataObject} instances contained in this object.
     *
     * @return the collection {@link DataObject} instances contained in this object.
     */
    @XmlElement(name = "data_object")
    protected List<DataObjectAsXML> getDataObjectAsXMLs() {
        return dataObjectAsXMLs;
    }

    private Map<String, DataObjectAsXML> getDataObjectByIdentity() {
        if (null == dataObjectByIdentity) {
            dataObjectByIdentity = new HashMap<>();
            for (DataObjectAsXML dataObjectAsXML : dataObjectAsXMLs) {
                dataObjectByIdentity.put(dataObjectAsXML.getIdentity(),
                                         dataObjectAsXML);
            }
        }
        return dataObjectByIdentity;
    }

    /**
     * Recovers the values of the {@link DataObjectImpl} instances in this object.
     *
     * @throws DataFlowException
     *             when a {@link DataObjectImpl} can not be successfully recovered.
     */
    void recover() {
        final Collection<? extends DataObjectImpl> dataObjects = getDataObjects();
        final Map<String, Object> values = new HashMap<>();
        for (DataObjectImpl dataObject : dataObjects) {
            final String identity = dataObject.getIdentity();
            final Object value = recoverValue(identity);
            if (null != value) {
                values.put(identity,
                           value);
            }
        }
        setRecoverValues(values);
    }

    private Object recoverValue(String identity) {
        final DataObjectAsXML dataObject = getDataObjectByIdentity().get(identity);
        final Object value = dataObject.getValue();
        if (null == value) {
            return null;
        }
        if (value instanceof ValueAsString) {
            return ((ValueAsString) value).getObject();
        } else if (value instanceof ValueCollection) {
            final List<Object> values = unwrapCollection((ValueCollection) value);
            return values;
        }
        return value;
    }

    /**
     * Sets the collection {@link DataObject} instances contained in this object
     * 
     * @param dataObjects
     *            the collection {@link DataObject} instances contained in this
     *            object
     */
    protected void getDataObjectAsXMLs(List<DataObjectAsXML> dataObjects) {
        dataObjectAsXMLs = dataObjects;
    }

    void setDataElements(Collection<? extends DataObjectImpl> dataObjects,
                         Class<?>[] persistentClasses) {
        setDataObjects(dataObjects);
        this.persistentClasses = persistentClasses;
    }

    /**
     * Builds a {@link List} instance from the provided {@link ValueCollection}
     * instance.
     *
     * @param collection
     *            the {@link ValueCollection} instance from which to build the
     *            {@link List} instance.
     *
     * @return the {@link List} instance built from the provided
     *         {@link ValueCollection} instance.
     */
    private static List<Object> unwrapCollection(ValueCollection collection) {
        final List<Object> values = new ArrayList<>();
        for (Object value : collection.getContent()) {
            if (value instanceof NullValue) {
                values.add(null);
            } else if (value instanceof ValueAsString) {
                values.add(((ValueAsString) value).getObject());
            } else if (value instanceof ValueCollection) {
                values.add(unwrapCollection((ValueCollection) value));
            } else {
                values.add(value);
            }
        }
        return values;
    }
}
