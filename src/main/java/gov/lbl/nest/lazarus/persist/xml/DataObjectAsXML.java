package gov.lbl.nest.lazarus.persist.xml;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import gov.lbl.nest.lazarus.data.DataObjectImpl;

/**
 * This class preserve the value of a {@link DataObjectImpl} in an XML file.
 *
 * @author patton
 */
@XmlType(propOrder = { "identity",
                       "value" })
class DataObjectAsXML {

    /**
     * The unique identity of this object with in the context of the structure.
     */
    private String identity;

    /**
     * The value of this object
     */
    private Object value;

    /**
     * Creates an instance of this class.
     */
    protected DataObjectAsXML() {
    }

    /**
     * Returns the unique identity of this object with in the context of the
     * structure.
     *
     * @return the unique identity of this object with in the context of the
     *         structure.
     */
    @XmlElement
    protected String getIdentity() {
        return identity;
    }

    @XmlAnyElement(lax = true)
    protected Object getValue() {
        return value;
    }

    /**
     * Sets the unique identity of this object with in the context of the structure.
     * 
     * @param identity
     *            the unique identity of this object with in the context of the
     *            structure.
     */
    protected void setIdentity(String identity) {
        this.identity = identity;
    }

    protected void setValue(Object value) {
        this.value = value;
    }
}
