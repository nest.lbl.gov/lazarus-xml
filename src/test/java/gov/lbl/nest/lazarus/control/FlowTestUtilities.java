package gov.lbl.nest.lazarus.control;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

import gov.lbl.nest.lazarus.data.ItemDefinitionImpl;
import gov.lbl.nest.lazarus.execution.ExecutableProcess;

/**
 * This class contains data and code used across multiple flow tests.
 *
 * @author patton
 */
public class FlowTestUtilities {

    /**
     * The String instance used to identify the item definition of a {@link String}
     * instance.
     */
    protected static final String STRING_ID = "IDENTITY_9";

    /**
     * The String instance used to identify the item definition of an
     * {@link Integer} instance.
     */
    protected static final String INT_ID = "IDENTITY_1";

    /**
     * The name of the output data element.
     */
    protected static final String INTERFACE_CLASS = "gov.lbl.nest.lazarus.process.MockStringManipulation";

    /**
     * The name of the input data element.
     */
    protected static final String INTERFACE_NAME = "interface";

    /**
     * The String instance used to identify the input data element.
     */
    protected static final String INTERFACE_ID = "IDENTITY_10";

    /**
     * The name of the input data element.
     */
    public static final String INPUT_NAME = "input";

    /**
     * The String instance used to identify the input data element.
     */
    protected static final String INPUT_ID = "IDENTITY_2";
    /**
     * The name of the input data element.
     */
    public static final String INPUT_1_NAME = "input_1";

    /**
     * The String instance used to identify the input data element.
     */
    protected static final String INPUT_1_ID = "IDENTITY_17";

    /**
     * The name of the input data element.
     */
    public static final String INPUT_2_NAME = "input_2";

    /**
     * The String instance used to identify the input data element.
     */
    protected static final String INPUT_2_ID = "IDENTITY_19";

    /**
     * The name of the input set element.
     */
    public static final String INPUT_SET_NAME = "inputSet";

    /**
     * The String instance used to identify the input set element.
     */
    protected static final String INPUT_SET_ID = "IDENTITY_14";

    /**
     * The name of the input message element.
     */
    public static final String IN_MESSAGE_NAME = "inMessage";

    /**
     * The String instance used to identify the input message element.
     */
    protected static final String IN_MESSAGE_ID = "IDENTITY_12";

    /**
     * The value of the input data element.
     */
    public static final int INPUT_INT_VALUE = 1;

    /**
     * The value of the input data element.
     */
    public static final String INPUT_STRING_VALUE = "one two three";

    /**
     * The value of the input data element.
     */
    public static final List<String> INPUT_STRING_COLLECTION_VALUE = Arrays.asList(new String[] { INPUT_STRING_VALUE,
                                                                                                  "four five six",
                                                                                                  "seven eight nine" });

    /**
     * The {@link ItemDefinitionImpl} instance representing a {@link String}
     * instance.
     */
    protected static ItemDefinitionImpl STRING_DEFINITION;

    /**
     * The {@link ItemDefinitionImpl} instance representing a collection of
     * {@link String} instances.
     */
    protected static ItemDefinitionImpl STRING_COLLECTION_DEFINITION;

    static {
        try {
            STRING_DEFINITION = new ItemDefinitionImpl(STRING_ID,
                                                       String.class.getName(),
                                                       false);
            STRING_COLLECTION_DEFINITION = new ItemDefinitionImpl(STRING_ID,
                                                                  String.class.getName(),
                                                                  true);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * The name of the output data element.
     */
    protected static final String OPERATION_METHOD = "split";

    /**
     * The name of the output data element.
     */
    protected static final String OPERATION_1_METHOD = "firstWord";

    /**
     * The name of the output data element.
     */
    protected static final String OPERATION_2_METHOD = "triplicate";

    /**
     * The name of the operation element.
     */
    protected static final String OPERATION_NAME = "operation";

    /**
     * The name of the operation element.
     */
    protected static final String OPERATION_1_NAME = "operation_1";

    /**
     * The name of the operation element.
     */
    protected static final String OPERATION_2_NAME = "operation_2";

    /**
     * The String instance used to identify operation element.
     */
    protected static final String OPERATION_ID = "IDENTITY_11";

    /**
     * The String instance used to identify operation element.
     */
    protected static final String OPERATION_1_ID = "IDENTITY_21";

    /**
     * The String instance used to identify operation element.
     */
    protected static final String OPERATION_2_ID = "IDENTITY_22";

    /**
     * The name of the output data element.
     */
    public static final String OUTPUT_NAME = "output";

    /**
     * The String instance used to identify the output data element.
     */
    protected static final String OUTPUT_ID = "IDENTITY_3";

    /**
     * The name of the output data element.
     */
    public static final String OUTPUT_1_NAME = "output_1";

    /**
     * The String instance used to identify the output data element.
     */
    protected static final String OUTPUT_1_ID = "IDENTITY_18";

    /**
     * The name of the output data element.
     */
    public static final String OUTPUT_2_NAME = "output_2";

    /**
     * The String instance used to identify the output data element.
     */
    protected static final String OUTPUT_2_ID = "IDENTITY_20";

    /**
     * The name of the output data element.
     */
    public static final String OUTPUT_SET_NAME = "outputSet";

    /**
     * The String instance used to identify the output data element.
     */
    protected static final String OUTPUT_SET_ID = "IDENTITY_15";

    /**
     * The value of the output data element.
     */
    public static final int OUTPUT_INT_VALUE = 2;

    /**
     * The name of the output message element.
     */
    public static final String OUT_MESSAGE_NAME = "inMessage";

    /**
     * The String instance used to identify the output message element.
     */
    protected static final String OUT_MESSAGE_ID = "IDENTITY_13";

    /**
     * The value of the output data element.
     */
    public static final String OUTPUT_STRING_VALUE = INPUT_STRING_VALUE.split(" ")[0];

    /**
     * The value of the output data element.
     */
    public static final String OUTPUT_ALT_STRING_VALUE = OUTPUT_STRING_VALUE + " "
                                                         + OUTPUT_STRING_VALUE
                                                         + " "
                                                         + OUTPUT_STRING_VALUE;

    /**
     * The value of the output data element.
     */
    public static final List<String> OUTPUT_STRING_COLLECTION_VALUE = Arrays.asList(INPUT_STRING_VALUE.split(" "));

    /**
     * The value of the output data element.
     */
    public static final List<String> OUTPUT_1_STRING_COLLECTION_VALUE = Arrays.asList("one",
                                                                                      "four",
                                                                                      "seven");

    /**
     * The String instance used to identify the input output specification.
     */
    protected static final String IOSPEC_ID = "IDENTITY_4";

    /**
     * The String instance used to identify the data input association.
     */
    protected static final String INPUT_ASSOC_ID = "IDENTITY_5";

    /**
     * The String instance used to identify the data input association.
     */
    protected static final String OUTPUT_ASSOC_ID = "IDENTITY_6";

    /**
     * The name the data object containing the input data.
     */
    protected static final String INPUT_DATA_NAME = "input_data";

    /**
     * The String instance used to identify the data object containing the input
     * data.
     */
    protected static final String INPUT_DATA_ID = "IDENTITY_7";

    /**
     * The name the data object containing the output data.
     */
    protected static final String OUTPUT_DATA_NAME = "output_data";

    /**
     * The String instance used to identify the data object containing the input
     * data.
     */
    protected static final String OUTPUT_DATA_ID = "IDENTITY_8";

    /**
     * The String instance used to identify the loop characteristics.
     */
    protected static final String LOOP_ID = "IDENTITY_16";

    /**
     * The identity to use from the Start event.
     */
    protected static final String ACTIVITY_ID = "NODE_2";

    /**
     * The name used from the Start event.
     */
    protected static final String ACTIVITY_NAME = "activity_0";

    /**
     * The identity to use from the Start event.
     */
    protected static final String ACTIVITY_1_ID = "NODE_6";

    /**
     * The name used from the Start event.
     */
    protected static final String ACTIVITY_1_NAME = "activity_1";

    /**
     * The identity to use from the Start event.
     */
    protected static final String END_ID = "NODE_1";

    /**
     * The name used from the Start event.
     */
    protected static final String END_NAME = "end";

    /**
     * The identity to use from the Fork gateway.
     */
    protected static final String FORK_ID = "NODE_5";

    /**
     * The name used from the Fork gateway.
     */
    protected static final String FORK_NAME = "fork";

    /**
     * The identity to use from the Join gateway.
     */
    protected static final String JOIN_ID = "NODE_7";

    /**
     * The name used from the Join gateway.
     */
    protected static final String JOIN_NAME = "join";

    /**
     * The identity to use from the Merge gateway.
     */
    protected static final String MERGE_ID = "NODE_4";

    /**
     * The name used from the Merge gateway.
     */
    protected static final String MERGE_NAME = "merge";

    /**
     * The identity to use from the Process object.
     */
    protected static final String PROCESS_ID = "PROCESS_1";

    /**
     * The identity to use from the Process object.
     */
    protected static final String PROCESS_NAME = "process";

    /**
     * The identity to use from the Service task.
     */
    protected static final String SERVICE_ID = "NODE_8";

    /**
     * The name used from the Service task.
     */
    protected static final String SERVICE_NAME = "service_task";

    /**
     * The identity to use from the Split gateway.
     */
    protected static final String SPLIT_ID = "NODE_3";

    /**
     * The name used from the Split gateway.
     */
    protected static final String SPLIT_NAME = "split";

    /**
     * The identity to use from the Start event.
     */
    protected static final String START_ID = "NODE_0";

    /**
     * The name used from the Start event.
     */
    protected static final String START_NAME = "start";

    /**
     * The sequence of labels for the test {@link ControlScope} instances.
     */
    protected static final List<String> LABELS = new ArrayList<>();

    static {
        for (int i = 0;
             i != 19;
             ++i) {
            LABELS.add("label_" + Integer.valueOf(i));
        }
    }

    /**
     * Confirms the termination of an instance is as expected.
     *
     * @param terminated
     *            the {@link AtomicBoolean} instance containing the current
     *            termination state of a process.
     *
     * @throws InterruptedException
     *             when this method is interrupted.
     */
    protected static void confirmTermination(AtomicBoolean terminated) throws InterruptedException {
        if (!terminated.get()) {
            synchronized (terminated) {
                if (!terminated.get()) {
                    terminated.wait();
                }
            }
        }
        assertTrue(terminated.get());
    }

    /**
     * Confirms the termination of an instance is as expected and that the
     * appropriate collection of {@link String} instance are output.
     *
     * @param tracker
     *            the {@link TestTracker} instance containing the
     *            {@link ControlScope} instance and its expectations.
     * @param listener
     *            the {@link MockTerminationListener} for all of the
     *            {@link ControlScope} instances returned.
     *
     * @return <code>true</code> when no more confirmation should be run.
     *
     * @throws InterruptedException
     *             when this method is interrupted.
     */
    static boolean confirmTermination(TestTracker tracker,
                                      MockTerminationListener listener) throws InterruptedException {
        final ExecutableProcess process = tracker.process;
        confirmTermination(process.getTerminated());
        if (!tracker.success) {
            assertTrue((listener.getFailures()).contains(process));
            return true;
        }
        assertTrue((listener.getSuccesses()).contains(process));
        return false;
    }

    /**
     * Confirms the termination of an instance is as expected and that the
     * appropriate {@link MockActivityBody} instance has been executed.
     *
     * @param tracker
     *            the {@link TestTracker} instance containing the
     *            {@link ControlScope} instance and its expectations.
     * @param listener
     *            the {@link MockTerminationListener} for all of the
     *            {@link ControlScope} instances
     * @param body
     *            the {@link MockActivityBody} instance from the test.
     *
     * @throws InterruptedException
     *             when this method is interrupted.
     */
    protected static void confirmTermination(TestTracker tracker,
                                             MockTerminationListener listener,
                                             MockActivityBody body) throws InterruptedException {
        if (confirmTermination(tracker,
                               listener)) {
            return;
        }

//        final ExecutableProcess process = tracker.process;
//        if (null != body) {
//            assertEquals(tracker.activity,
//                         (body.getSeen()).contains(tracker.controlScope));
//        }
    }

    /**
     * Confirms the termination of an instance is as expected and that the
     * appropriate {@link MockActivityBody} instance has been executed.
     *
     * @param tracker
     *            the {@link TestTracker} instance containing the
     *            {@link ControlScope} instance and its expectations.
     * @param listener
     *            the {@link MockTerminationListener} for all of the
     *            {@link ControlScope} instances
     * @param body
     *            the {@link MockActivityBody} instance from the test.
     * @param bodyAlt
     *            the alternative {@link MockActivityBody} instance from the test.
     *
     * @throws InterruptedException
     *             when this method is interrupted.
     */
    protected static void confirmTermination(TestTracker tracker,
                                             MockTerminationListener listener,
                                             MockActivityBody body,
                                             MockActivityBody bodyAlt) throws InterruptedException {
        if (confirmTermination(tracker,
                               listener)) {
            return;
        }

        final ExecutableProcess process = tracker.process;
        if (null != body) {
            assertEquals(tracker.activity,
                         (body.getSeen()).contains(process));
        }

        if (null != bodyAlt) {
            assertEquals(tracker.activity_1,
                         (bodyAlt.getSeen()).contains(process));
        }
    }

    /**
     * Confirms the termination of an instance is as expected and that the
     * appropriate collection of {@link String} instance are output.
     *
     * @param tracker
     *            the {@link TestTracker} instance containing the
     *            {@link ControlScope} instance and its expectations.
     * @param listener
     *            the {@link MockTerminationListener} for all of the
     *            {@link ControlScope} instances
     * @param outputName
     *            the name of the output to test.
     * @param expected
     *            the expected sequence of {@link String} instance that should be
     *            returned.
     *
     * @throws InterruptedException
     *             when this method is interrupted.
     */
    protected static void confirmTermination(TestTracker tracker,
                                             MockTerminationListener listener,
                                             String outputName,
                                             List<String> expected) throws InterruptedException {
        if (confirmTermination(tracker,
                               listener)) {
            return;
        }

        final ExecutableProcess process = tracker.process;
        final Map<String, Object> results = process.getValues();
        assertEquals(2,
                     results.size());
        final List<?> result = (List<?>) results.get(outputName);
        assertNotNull(result);
        assertEquals(expected.size(),
                     result.size());
        final Iterator<?> iterator = result.iterator();
        for (String string : expected) {
            assertEquals(string,
                         (iterator.next()));
        }
    }

    /**
     * Create a minimal fork and join workflow.
     *
     * @param body
     *            the {@link ControlOnlyActivityBody} instance to use on one fork.
     * @param body1
     *            the {@link ControlOnlyActivityBody} instance to use on the other
     *            fork.
     *
     * @return the created {@link ControlOnlyProcessImpl} instance.
     */
    protected ControlOnlyProcessImpl createMinimalForkAndJoin(final ControlOnlyActivityBody body,
                                                              final ControlOnlyActivityBody body1) {
        final List<FlowElementImpl> flowElements = new ArrayList<>();
        final StartEventImpl start = new StartEventImpl(START_NAME,
                                                        START_ID);
        flowElements.add(start);
        final FlowNodeImpl fork = new ParallelGatewayImpl(FORK_NAME,
                                                          FORK_ID,
                                                          null);
        flowElements.add(fork);
        final ControlOnlyActivityImpl activity = new ControlOnlyActivityImpl(ACTIVITY_NAME,
                                                                             ACTIVITY_ID,
                                                                             body);
        flowElements.add(activity);
        final ControlOnlyActivityImpl activity1 = new ControlOnlyActivityImpl(ACTIVITY_1_NAME,
                                                                              ACTIVITY_1_ID,
                                                                              body1);
        flowElements.add(activity1);
        final FlowNodeImpl join = new ParallelGatewayImpl(JOIN_NAME,
                                                          JOIN_ID,
                                                          null);
        flowElements.add(join);
        final EndEventImpl end = new EndEventImpl(END_NAME,
                                                  END_ID);
        flowElements.add(end);
        flowElements.add(new SequenceFlowImpl(null,
                                              null,
                                              start,
                                              fork));
        flowElements.add(new SequenceFlowImpl(null,
                                              null,
                                              fork,
                                              activity));
        flowElements.add(new SequenceFlowImpl(null,
                                              null,
                                              fork,
                                              activity1));
        flowElements.add(new SequenceFlowImpl(null,
                                              null,
                                              activity,
                                              join));
        flowElements.add(new SequenceFlowImpl(null,
                                              null,
                                              activity1,
                                              join));
        flowElements.add(new SequenceFlowImpl(null,
                                              null,
                                              join,
                                              end));
        final ControlOnlyProcessImpl process = new ControlOnlyProcessImpl(PROCESS_NAME,
                                                                          PROCESS_ID,
                                                                          start,
                                                                          flowElements);
        return process;
    }

    /**
     * Create a minimal activity workflow.
     *
     * @param body
     *            the {@link ControlOnlyActivityBody} instance to use.
     *
     * @return the created {@link ControlOnlyProcessImpl} instance.
     */
    protected ControlOnlyProcessImpl minimalActivity(ControlOnlyActivityBody body) {
        final List<FlowElementImpl> flowElements = new ArrayList<>();
        final StartEventImpl start = new StartEventImpl(START_NAME,
                                                        START_ID);
        flowElements.add(start);
        final ControlOnlyActivityImpl activity = new ControlOnlyActivityImpl(ACTIVITY_NAME,
                                                                             ACTIVITY_ID,
                                                                             body);
        flowElements.add(activity);
        final EndEventImpl end = new EndEventImpl(END_NAME,
                                                  END_ID);
        flowElements.add(end);
        flowElements.add(new SequenceFlowImpl(null,
                                              null,
                                              start,
                                              activity));
        flowElements.add(new SequenceFlowImpl(null,
                                              null,
                                              activity,
                                              end));
        final ControlOnlyProcessImpl process = new ControlOnlyProcessImpl(PROCESS_NAME,
                                                                          PROCESS_ID,
                                                                          start,
                                                                          flowElements);
        return process;
    }

    /**
     * Overlays the src directory tree to the dst one.
     *
     * @param src
     *            the source directory tree
     * @param dst
     *            the destination directory tree.
     *
     * @throws IOException
     *             if the copy fails.
     */
    protected void overlayDirectory(Path src,
                                    Path dst) throws IOException {
        try (final Stream<Path> contents = Files.walk(src)) {
            contents.forEach(source -> {
                Path target = dst.resolve(src.relativize(source));
                try {
                    Files.copy(source,
                               target);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

}
