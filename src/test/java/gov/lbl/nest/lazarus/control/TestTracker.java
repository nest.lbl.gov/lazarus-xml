package gov.lbl.nest.lazarus.control;

import gov.lbl.nest.lazarus.execution.ExecutableProcess;

/**
 * This class is used to track an {@link ControlScope} instance through its
 * tests.
 *
 * @author patton
 *
 */
public class TestTracker {

    /**
     * The {@link ExecutableProcess} instance being tested.
     */
    public ExecutableProcess process;

    /**
     * <code>true</code> is the {@link ControlScope} instance should terminate
     * successfully.
     */
    public boolean success = true;

    /**
     * <code>true</code> is the {@link ControlScope} instance should have been seen
     * by the main activity.
     */
    public boolean activity = false;

    /**
     * <code>true</code> is the {@link ControlScope} instance should have been seen
     * by the secondary activity.
     */
    public boolean activity_1 = false;

}
